package Mapa;

import java.awt.Container;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Point;
import java.util.PriorityQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.util.HashSet;
import java.util.List;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;

import AGM.UnionFind;
import grafos.Arista;
import grafos.Grafo;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;

public class Mapa
{

	private static final Point2D GreatCircle = null;
	private JFrame frame;
	private JPanel panelMapa;
	private JPanel panelControles;
	private JMapViewer _mapa;
	private ArrayList<Coordinate> _lasCoordenadas;
	private JButton btnEliminar;
	private MapPolygonImpl _poligono;
	private JButton btnDibujarPolgono ;
	private Grafo grafo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() {
				try {
					Mapa window = new Mapa();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Mapa() 
	{
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() 
	{
		frame = new JFrame();
		frame.setBounds(100, 100, 725, 506);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		panelMapa = new JPanel();
		panelMapa.setBounds(10, 11, 437, 446);
		frame.getContentPane().add(panelMapa);
		
		panelControles = new JPanel();
		panelControles.setBounds(457, 11, 242, 446);
		frame.getContentPane().add(panelControles);		
		panelControles.setLayout(null);
		
		_mapa = new JMapViewer();
		_mapa.setDisplayPosition(new Coordinate(-34.521, -58.7008), 15);
				
		panelMapa.add(_mapa);

		detectarCoordenadas();
		dibujarPoligono();
		eliminarPoligono();	
		mostrarArbol();
		prueba();

		//arbolGeneradorKruskal(_lasCoordenadas);
		
		
	}
	
	private void detectarCoordenadas() 
	{
		_lasCoordenadas = new ArrayList<Coordinate>();
				
		_mapa.addMouseListener(new MouseAdapter() 
		{
			@Override
			public void mouseClicked(MouseEvent e) 
			{
			if (e.getButton() == MouseEvent.BUTTON1)
			{
				Coordinate markeradd = (Coordinate)
				_mapa.getPosition(e.getPoint());
				_lasCoordenadas.add(markeradd);
				String nombre = JOptionPane.showInputDialog("Nombre: ");
				
				_mapa.addMapMarker(new MapMarkerDot(nombre, markeradd));
			}}
		});
	}
	
	

	private void dibujarPoligono() 
	{
		btnDibujarPolgono = new JButton("Dibujar Pol\u00EDgono");
		btnDibujarPolgono.setBounds(10, 11, 195, 23);
		btnDibujarPolgono.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				_poligono = new MapPolygonImpl(_lasCoordenadas);
				_mapa.addMapPolygon(_poligono);
			}
		});
	}

	private void eliminarPoligono() 
	{
		btnEliminar = new JButton("Eliminar Pol�gono");
		btnEliminar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				 _mapa.removeMapPolygon(_poligono);
			}
		});
		btnEliminar.setBounds(10, 64, 195, 23);
		panelControles.add(btnEliminar);
		panelControles.add(btnDibujarPolgono);
	}	
	
	
	private void mostrarArbol() 
	{
	    JButton btnMostrarArbol = new JButton("Mostrar Árbol");
	    btnMostrarArbol.setBounds(10, 117, 195, 23);
	    btnMostrarArbol.addActionListener(new ActionListener() 
	    {
	        public void actionPerformed(ActionEvent arg0) 
	        {
	            List<Arista> arbol = arbolGeneradorKruskal(_lasCoordenadas);
	         
	            StringBuilder sb = new StringBuilder();
	            for (Arista arista : arbol) {
	                sb.append(arista.toString()).append("\n");
	                
	            }
	            JOptionPane.showMessageDialog(frame, sb.toString(), "Árbol Generador Mínimo", JOptionPane.INFORMATION_MESSAGE);
	        }
	    });
	    panelControles.add(btnMostrarArbol);
	}
	
	List<Arista> arbolGeneradorKruskal(ArrayList<Coordinate> _lasCoordenadas) {
		Grafo _grafo = new Grafo(_lasCoordenadas.size());
	    for (int i = 0; i < _lasCoordenadas.size() - 1; i++) {
	        Coordinate coordenada1 = _lasCoordenadas.get(i);
	        Coordinate coordenada2 = _lasCoordenadas.get(i + 1);
	        double peso = calcularDistancia(coordenada1, coordenada2);
	        _grafo.agregarArista(i, i + 1, peso);
	    }
	    return _grafo.kruskal();
	}
	
	double calcularDistancia(Coordinate coordenada1, Coordinate coordenada2) {
	    double x1 = coordenada1.getLat();
	    double y1 = coordenada1.getLon();
	    double x2 = coordenada2.getLat();
	    double y2 = coordenada2.getLon();

	    double distancia = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
	    return distancia;
	}
	
	 public void prueba() {
	     // Crear el grafo y agregar aristas
		 Grafo grafo = new Grafo(3);
	       
	        grafo.agregarArista(0, 1, 5);
	        grafo.agregarArista(1, 2, 3);
	        grafo.agregarArista(0, 2, 2);

	     
	        List<Arista> arbolGeneradorMinimo = grafo.kruskal();

	     // Verificar que el árbol generador mínimo no contiene ciclos
	     for (Arista arista : arbolGeneradorMinimo) {
	         int origen = arista.getOrigen();
	         int destino = arista.getDestino();
	         System.out.println(origen);
	         System.out.println(destino);
	
	
	     }
	     }
}
