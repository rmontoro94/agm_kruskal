package AGM;

public class UnionFind {
	 int[] parent;
	    int[] rank;

	    public UnionFind(int numVertices) {
	        parent = new int[numVertices];
	        rank = new int[numVertices];

	        for (int i = 0; i < numVertices; i++) {
	            parent[i] = i;
	            rank[i] = 0;
	        }
	    }

	    public int find(int x) {
	        if (parent[x] != x) {
	            parent[x] = find(parent[x]);
	        }
	        return parent[x];
	    }

	    public void union(int x, int y) {
	        int rootX = find(x);
	        int rootY = find(y);
	        
	        if (rank[rootX] < rank[rootY]) {
	            parent[rootX] = rootY;
	        } else if (rank[rootX] > rank[rootY]) {
	            parent[rootY] = rootX;
	        } else {
	            parent[rootY] = rootX;
	            rank[rootX]++;
	        }
	    }
	    
	    public boolean connected(int x, int y) {
	        int rootX = find(x);
	        int rootY = find(y);
	        return rootX == rootY;
	    }
	}


