package grafos;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.List;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import AGM.UnionFind;

public class KruskalTest {

	
	 @Test
	    public void testAristasOrdenadas() {
	        // Crear grafo de prueba con nodos y aristas
	        Grafo grafo = new Grafo(3);
	       
	        grafo.agregarArista(0, 1, 5);
	        grafo.agregarArista(1, 2, 3);
	        grafo.agregarArista(0, 2, 2);

	        // Llamar al método de ordenación de aristas
	        
	        List<Arista> aristasOrdenadas = grafo.kruskal();

	        // Verificar que las aristas se han ordenado correctamente
	        List<Arista> aristasEsperadas = new ArrayList<>();
	        
	        aristasEsperadas.add(new Arista(0, 2, 2));
	        aristasEsperadas.add(new Arista(1, 2, 3));
	       

	        assertArrayEquals(aristasEsperadas.toArray(), aristasOrdenadas.toArray());
	    }
	 
	 @Test
	    public void numeroAristas() {
	    // Set Up
	     Grafo grafo = new Grafo(3);
	     grafo.agregarArista(0, 1, 1);
	     grafo.agregarArista(1, 2, 2);
	     grafo.agregarArista(2, 0, 3);
	     // exercise
	     List<Arista> arbolGeneradorMinimo = grafo.kruskal();
	     // verify
	     assertEquals(arbolGeneradorMinimo.size(), grafo.tamano()-1);
	    }
	 
	 @Test
	 public void sinCiclos() {
	     // Crear el grafo y agregar aristas
		 Grafo grafo = new Grafo(3);
	       
	        grafo.agregarArista(0, 1, 5);
	        grafo.agregarArista(1, 2, 3);
	        grafo.agregarArista(0, 2, 2);

	     
	        List<Arista> arbolGeneradorMinimo = grafo.kruskal();

	     // Verificar que el árbol generador mínimo no contiene ciclos
	     UnionFind unionFind = new UnionFind(3);
	     for (Arista arista : arbolGeneradorMinimo) {
	         int origen = arista.getOrigen();
	         int destino = arista.getDestino();
	         assertFalse(unionFind.connected(origen, destino));
	         unionFind.union(origen, destino);
	     }
	 }
	 @Test
	 public void testAristasUnicas() {
	     // Crear el grafo y agregar aristas
		 Grafo grafo = new Grafo(3);
	       
	        grafo.agregarArista(0, 1, 5);
	        grafo.agregarArista(1, 2, 3);
	        grafo.agregarArista(0, 2, 2);

	     // Llamar al método kruskal()
	        List<Arista> arbolGeneradorMinimo = grafo.kruskal();

	     // Verificar que no hay aristas duplicadas en el árbol generador mínimo
	     Set<String> aristasUnicas = new HashSet<>();
	     for (Arista arista : arbolGeneradorMinimo) {
	         String aristaString = arista.getOrigen() + "-" + arista.getDestino();
	         assertFalse(aristasUnicas.contains(aristaString));
	         aristasUnicas.add(aristaString);
	     }
	 }
}
