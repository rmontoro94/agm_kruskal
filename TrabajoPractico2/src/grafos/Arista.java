package grafos;

import java.util.Objects;

public class Arista {
	private int origen;
	private int destino;
	
	
	double peso;
	
	
	public Arista (int origen, int destino, double peso) {
		this.origen = origen;
		this.destino = destino;
		this.peso = peso;
	}
	
	public int getOrigen() {
		return origen;
	}
	
	public int getDestino() {
		return destino;
	}
	
	public double getPeso() {
		return peso;
	}
	
	@Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Arista other = (Arista) obj;
        return destino == other.destino && origen == other.origen
                && Double.doubleToLongBits(peso) == Double.doubleToLongBits(other.peso);
    }
	@Override
    public String toString() {
        return "Arista [origen=" + origen + ", destino=" + destino + ", costo en pesos=" + peso + "]";
    }
	@Override
    public int hashCode() {
        return Objects.hash(destino, origen, peso);
    }
}
