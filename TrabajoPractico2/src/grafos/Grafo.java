package grafos;

import java.util.ArrayList;
import java.util.LinkedList;
import AGM.UnionFind;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class Grafo
{
	// Representamos el grafo por su matriz de adyacencia
	private ArrayList<HashSet<Integer>> _vecinos;
	private List<Arista> _aristas;

	// La cantidad de vertices esta predeterminada desde el constructor
	public Grafo(int vertices)
	{
		_vecinos = new ArrayList<HashSet<Integer>>();
		_aristas = new LinkedList<Arista>();
		for(int i=0; i<vertices; ++i)
			_vecinos.add(new HashSet<Integer>());
		
		
	}
	
	// Agregado de aristas
	public void agregarArista(int i, int j, double peso)
	{
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);
        
		_vecinos.get(i).add(j);
		_vecinos.get(j).add(i);	
       _aristas.add(new Arista(i, j, peso));
		
	}
	
	
	// Eliminacion de aristas
	public void eliminarArista(int i, int j)
	{
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);

		_vecinos.get(i).remove(j);
		_vecinos.get(j).remove(i);
	}

	// Informa si existe la arista especificada
	public boolean existeArista(int i, int j)
	{
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);

		return _vecinos.get(i).contains(j);
		
	}
	
	
	  public List<Arista> kruskal() {
	        List<Arista> arbolGenerador = new ArrayList<>();
	        Collections.sort(_aristas, Comparator.comparingDouble(a -> a.getPeso()));
	        
	        
	        UnionFind unionFind = new UnionFind(_vecinos.size());

	        for (Arista arista : _aristas) {
	            if (unionFind.find(arista.getOrigen()) != unionFind.find(arista.getDestino())) {
	                arbolGenerador.add(arista);
	                unionFind.union(arista.getOrigen(), arista.getDestino());
	            }
	        }

	        return arbolGenerador;
	    }
	
		


	// Cantidad de vertices
	public int tamano()
	{
		return _vecinos.size();
	}
	
	// Vecinos de un vertice
	public Set<Integer> vecinos(int i)
	{
		return _vecinos.get(i);
		
	}
	
	
	
	public int grado(int i)
	{
		verificarVertice(i);
		return vecinos(i).size();
	}
	
	// Verifica que sea un vertice valido
	private void verificarVertice(int i)
	{
		if( i < 0 )
			throw new IllegalArgumentException("El vertice no puede ser negativo: " + i);
		
		if( i >= tamano() )
			throw new IllegalArgumentException("Los vertices deben estar entre 0 y |V|-1: " + i);
	}

	// Verifica que i y j sean distintos
	private void verificarDistintos(int i, int j)
	{
		if( i == j )
			throw new IllegalArgumentException("No se permiten loops: (" + i + ", " + j + ")");
	}
	
	
}

